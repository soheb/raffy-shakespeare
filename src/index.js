import "@fontsource/unifrakturmaguntia"
import './style';
import App from './components/app';

export default App;
