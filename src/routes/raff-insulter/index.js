import style from "./style.css";
import raffster from '../../assets/raff.png';
import shakespeare from 'shakespeare-insult';
import { useState } from 'preact/hooks'

const Raff = () => {
    const [quote, setQuote] = useState(shakespeare.random());
    const refresh = () => {
        setQuote(shakespeare.random())
    }
    return (<div className="container text-center">
        <div className="row">
            <div className="col">
                <img src={raffster} className={style.raffImg}  alt="Raffy the cat, scowling at you" />
            </div>
        </div>
        <div className="row">
            <div className="col">
                Thou {quote}!
            </div>
        </div>
        <div className="row">
            <div className="col">
                <button type="button" className="btn btn-danger" onClick={refresh}>Möther, I crave violence</button>
            </div>
        </div>
    </div>)
};


export default Raff;